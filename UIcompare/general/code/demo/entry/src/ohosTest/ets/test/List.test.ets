/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// background
import Background01 from './generalAttributes/background/background01.test'
import Background02 from './generalAttributes/background/background02.test'
import Background03 from './generalAttributes/background/background03.test'
import Background04 from './generalAttributes/background/background04.test'
import Background05 from './generalAttributes/background/background05.test'
import Background06 from './generalAttributes/background/background06.test'
import Background07 from './generalAttributes/background/background07.test'
import Background08 from './generalAttributes/background/background08.test'
import Background09 from './generalAttributes/background/background09.test'
import Background10 from './generalAttributes/background/background10.test'
import Background11 from './generalAttributes/background/background11.test'
import Background12 from './generalAttributes/background/background12.test'
import Background13 from './generalAttributes/background/background13.test'
import Background14 from './generalAttributes/background/background14.test'
import Background15 from './generalAttributes/background/background15.test'
import Background16 from './generalAttributes/background/background16.test'
import Background17 from './generalAttributes/background/background17.test'

import LinearGradient01 from './generalAttributes/background/linearGradient/LinearGradient01.test'
import LinearGradient02 from './generalAttributes/background/linearGradient/LinearGradient02.test'
import LinearGradient03 from './generalAttributes/background/linearGradient/LinearGradient03.test'
import LinearGradient04 from './generalAttributes/background/linearGradient/LinearGradient04.test'
import LinearGradient05 from './generalAttributes/background/linearGradient/LinearGradient05.test'

import RadialGradient01 from './generalAttributes/background/radialGradient/radialGradient01.test'
import RadialGradient03 from './generalAttributes/background/radialGradient/radialGradient03.test'

import SweepGradient01 from './generalAttributes/background/sweepGradient/sweepGradient01.test'
import SweepGradient02 from './generalAttributes/background/sweepGradient/sweepGradient02.test'
import SweepGradient03 from './generalAttributes/background/sweepGradient/sweepGradient03.test'
import SweepGradient04 from './generalAttributes/background/sweepGradient/sweepGradient04.test'
import SweepGradient05 from './generalAttributes/background/sweepGradient/sweepGradient05.test'
import SweepGradient06 from './generalAttributes/background/sweepGradient/sweepGradient06.test'

// focusable
import FocusTest01 from './generalAttributes/focusable/FocusTest01.test'
import FocusTest02 from './generalAttributes/focusable/FocusTest02.test'
import FocusTest03 from './generalAttributes/focusable/FocusTest03.test'

// graphicTransformation
import RotateTest01 from './generalAttributes/graphicTransformation/RotateTest01.test'
import RotateTest02 from './generalAttributes/graphicTransformation/RotateTest02.test'
import ScaleTest02 from './generalAttributes/graphicTransformation/ScaleTest02.test'
import TranslateTest02 from './generalAttributes/graphicTransformation/TranslateTest02.test'

// matrix
import MatrixTest01 from './generalAttributes/matrix/MatrixTest01.test'
import MatrixTest02 from './generalAttributes/matrix/MatrixTest02.test'
import MatrixTest03 from './generalAttributes/matrix/MatrixTest03.test'
import MatrixTest04 from './generalAttributes/matrix/MatrixTest04.test'
import MatrixTest05 from './generalAttributes/matrix/MatrixTest05.test'
import MatrixTest06 from './generalAttributes/matrix/MatrixTest06.test'
import MatrixTest07 from './generalAttributes/matrix/MatrixTest07.test'
import MatrixTest08 from './generalAttributes/matrix/MatrixTest08.test'
import MatrixTest09 from './generalAttributes/matrix/MatrixTest09.test'
import MatrixTest10 from './generalAttributes/matrix/MatrixTest10.test'
import MatrixTest11 from './generalAttributes/matrix/MatrixTest11.test'
import MatrixTest12 from './generalAttributes/matrix/MatrixTest12.test'
import MatrixTest13 from './generalAttributes/matrix/MatrixTest13.test'
import MatrixTest14 from './generalAttributes/matrix/MatrixTest14.test'
import MatrixTest15 from './generalAttributes/matrix/MatrixTest15.test'

// overlayTest
import OverlayTest01 from './generalAttributes/overlayTest/OverlayTest01.test'
import OverlayTest02 from './generalAttributes/overlayTest/OverlayTest02.test'
import OverlayTest03 from './generalAttributes/overlayTest/OverlayTest03.test'
import OverlayTest04 from './generalAttributes/overlayTest/OverlayTest04.test'
import OverlayTest05 from './generalAttributes/overlayTest/OverlayTest05.test'

// shapeCutting
import ClipTest01 from './generalAttributes/shapeCutting/ClipTest01.test'
import ClipTest02 from './generalAttributes/shapeCutting/ClipTest02.test'
import ClipTest03 from './generalAttributes/shapeCutting/ClipTest03.test'
import ClipTest04 from './generalAttributes/shapeCutting/ClipTest04.test'
import ClipTest05 from './generalAttributes/shapeCutting/ClipTest05.test'
import ClipTest06 from './generalAttributes/shapeCutting/ClipTest06.test'
import ClipTest07 from './generalAttributes/shapeCutting/ClipTest07.test'
import ClipTest08 from './generalAttributes/shapeCutting/ClipTest08.test'
import ClipTest09 from './generalAttributes/shapeCutting/ClipTest09.test'
import ClipTest10 from './generalAttributes/shapeCutting/ClipTest10.test'
import MaskTest01 from './generalAttributes/shapeCutting/MaskTest01.test'
import MaskTest02 from './generalAttributes/shapeCutting/MaskTest02.test'
import MaskTest03 from './generalAttributes/shapeCutting/MaskTest03.test'
import MaskTest04 from './generalAttributes/shapeCutting/MaskTest04.test'
import MaskTest05 from './generalAttributes/shapeCutting/MaskTest05.test'

// visibility
import VisibilityTest01 from './generalAttributes/visibility/VisibilityTest01.test'

// zIndexFont
import ZIndexFont01 from './generalAttributes/zIndexFont/zIndexFontTest01.test'
import ZIndexFont06 from './generalAttributes/zIndexFont/fontColor01.test'
import ZIndexFont07 from './generalAttributes/zIndexFont/fontFamily.test'
import ZIndexFont08 from './generalAttributes/zIndexFont/fontSize01.test'
import ZIndexFont09 from './generalAttributes/zIndexFont/fontSize02.test'
import ZIndexFont10 from './generalAttributes/zIndexFont/fontStyle01.test'
import ZIndexFont11 from './generalAttributes/zIndexFont/fontStyle02.test'
import ZIndexFont12 from './generalAttributes/zIndexFont/fontWeight01.test'
import ZIndexFont13 from './generalAttributes/zIndexFont/fontWeight02.test'
import ZIndexFont14 from './generalAttributes/zIndexFont/fontWeight03.test'
import ZIndexFont15 from './generalAttributes/zIndexFont/fontSize03.test'
import ZIndexFont16 from './generalAttributes/zIndexFont/fontSize04.test'
import ZIndexFont17 from './generalAttributes/zIndexFont/fontColor02.test'
import ZIndexFont18 from './generalAttributes/zIndexFont/fontColor03.test'

// touchEvent
import TouchEvent01 from './generalEvents/touchTest01.test'
import TouchEvent02 from './generalEvents/touchTest02.test'
import TouchEvent03 from './generalEvents/touchTest03.test'
import TouchEvent04 from './generalEvents/touchTest04.test'
import TouchEvent05 from './generalEvents/touchTest05.test'

//border
import BorderTest001 from './generalAttributes/border/BorderTest001.test'
import BorderTest002 from './generalAttributes/border/BorderTest002.test'
import BorderTest003 from './generalAttributes/border/BorderTest003.test'

//imageEffect
import BackdropBlurTest001 from './generalAttributes/ImageEffect/BackdropBlurTest001.test'
import BlurTest001 from './generalAttributes/ImageEffect/BlurTest001.test'
import BrightnessTest001 from './generalAttributes/ImageEffect/BrightnessTest001.test'
import BrightnessTest002 from './generalAttributes/ImageEffect/BrightnessTest002.test'
import ColorBlendTest001 from './generalAttributes/ImageEffect/ColorBlendTest001.test'
import ContrastTest001 from './generalAttributes/ImageEffect/ContrastTest001.test'
import ContrastTest002 from './generalAttributes/ImageEffect/ContrastTest002.test'
import GrayscaleTest001 from './generalAttributes/ImageEffect/GrayscaleTest001.test'
import GrayscaleTest002 from './generalAttributes/ImageEffect/GrayscaleTest002.test'
import InvertTest001 from './generalAttributes/ImageEffect/InvertTest001.test'
import InvertTest002 from './generalAttributes/ImageEffect/InvertTest002.test'
import SaturateTest001 from './generalAttributes/ImageEffect/SaturateTest001.test'
import SaturateTest002 from './generalAttributes/ImageEffect/SaturateTest002.test'
import SepiaTest001 from './generalAttributes/ImageEffect/SepiaTest001.test'
import SepiaTest002 from './generalAttributes/ImageEffect/SepiaTest002.test'
import ShadowTest001 from './generalAttributes/ImageEffect/ShadowTest001.test'
import HueRotateTest001 from './generalAttributes/ImageEffect/HueRotateTest001.test'
import HueRotateTest002 from './generalAttributes/ImageEffect/HueRotateTest002.test'
import HueRotateTest003 from './generalAttributes/ImageEffect/HueRotateTest003.test'

//lay
import LayTest001 from './generalAttributes/lay/LayTest001.test'
import LayTest002 from './generalAttributes/lay/LayTest002.test'
import LayTest003 from './generalAttributes/lay/LayTest003.test'
import LayTest004 from './generalAttributes/lay/LayTest004.test'
import LayTest005 from './generalAttributes/lay/LayTest005.test'
import LayTest006 from './generalAttributes/lay/LayTest006.test'
import LayTest007 from './generalAttributes/lay/LayTest007.test'

//size
import WidthTest001 from './generalAttributes/size/WidthTest001.test'
import WidthTest002 from './generalAttributes/size/WidthTest002.test'
import HeightTest001 from './generalAttributes/size/HeightTest001.test'
import HeightTest002 from './generalAttributes/size/HeightTest002.test'
import ConstraintSizeTest001 from './generalAttributes/size/ConstraintSizeTest001.test'
import MarginTest001 from './generalAttributes/size/MarginTest001.test'
import MarginTest002 from './generalAttributes/size/MarginTest002.test'
import MarginTest003 from './generalAttributes/size/MarginTest003.test'
import MarginTest004 from './generalAttributes/size/MarginTest004.test'
import MarginTest005 from './generalAttributes/size/MarginTest005.test'
import MarginTest006 from './generalAttributes/size/MarginTest006.test'
import MarginTest007 from './generalAttributes/size/MarginTest007.test'
import MarginTest008 from './generalAttributes/size/MarginTest008.test'
import PaddingTest001 from './generalAttributes/size/PaddingTest001.test'
import PaddingTest002 from './generalAttributes/size/PaddingTest002.test'
import PaddingTest003 from './generalAttributes/size/PaddingTest003.test'
import PaddingTest004 from './generalAttributes/size/PaddingTest004.test'
import PaddingTest005 from './generalAttributes/size/PaddingTest005.test'
import PaddingTest006 from './generalAttributes/size/PaddingTest006.test'
import SizeTest002 from './generalAttributes/size/SizeTest002.test'
import SizeTest001 from './generalAttributes/size/SizeTest001.test'

import AlignTest from './generalAttributes/loc/AlignTest.test'
import AlignTest002 from './generalAttributes/loc/AlignTest002.test'
import AlignRulesTest from './generalAttributes/loc/AlignRulesTest.test'
import MarkAnchorTest001 from './generalAttributes/loc/MarkAnchorTest001.test'
import MarkAnchorTest002 from './generalAttributes/loc/MarkAnchorTest002.test'
import OffsetTest from './generalAttributes/loc/OffsetTest.test'
import PositionTest from './generalAttributes/loc/PositionTest.test'

//response
import ResponseRegionTest001 from './generalAttributes/ResponseRegion/ResponseRegionTest001.test'
import ResponseRegionTest002 from './generalAttributes/ResponseRegion/ResponseRegionTest002.test'
import ResponseRegionTest003 from './generalAttributes/ResponseRegion/ResponseRegionTest003.test'
import ResponseRegionTest004 from './generalAttributes/ResponseRegion/ResponseRegionTest004.test'
import ResponseRegionTest005 from './generalAttributes/ResponseRegion/ResponseRegionTest005.test'
import ResponseRegionTest006 from './generalAttributes/ResponseRegion/ResponseRegionTest006.test'
import ResponseRegionTest007 from './generalAttributes/ResponseRegion/ResponseRegionTest007.test'
import ResponseRegionTest008 from './generalAttributes/ResponseRegion/ResponseRegionTest008.test'

//click
import ClickTest001 from './generalEvents/ClickTest001.test'
import ClickTest002 from './generalEvents/ClickTest002.test'
import ClickTest003 from './generalEvents/ClickTest003.test'

//flex
import FlexAlignContentTest001 from './generalAttributes/Flex/FlexAlignContentTest001.test'
import FlexAlignItemsTest001 from './generalAttributes/Flex/FlexAlignItemsTest001.test'
import FlexAlignSelfTest001 from './generalAttributes/Flex/FlexAlignSelfTest001.test'
import FlexBasisTest001 from './generalAttributes/Flex/FlexBasisTest001.test'
import FlexBasisTest002 from './generalAttributes/Flex/FlexBasisTest002.test'
import FlexBasisTest003 from './generalAttributes/Flex/FlexBasisTest003.test'
import FlexBasisTest004 from './generalAttributes/Flex/FlexBasisTest004.test'
import FlexDirectionTest001 from './generalAttributes/Flex/FlexDirectionTest001.test'
import FlexDisplayPriority001 from './generalAttributes/Flex/FlexDisplayPriorityTest001.test'
import FlexGrowTest001 from './generalAttributes/Flex/FlexGrowTest001.test'
import FlexGrowTest002 from './generalAttributes/Flex/FlexGrowTest002.test'
import FlexGrowTest003 from './generalAttributes/Flex/FlexGrowTest003.test'
import FlexJustifyContentTest001 from './generalAttributes/Flex/FlexJustifyContentTest001.test'
import FlexLayoutWeight001 from './generalAttributes/Flex/FlexLayoutWeight001.test'
import FlexLayoutWeight002 from './generalAttributes/Flex/FlexLayoutWeight002.test'

export default function testsuite() {

  Background01()
  Background02()
  Background03()
  Background04()
  Background05()
  Background06()
  Background07()
  Background08()
  Background09()
  Background10()
  Background11()
  Background12()
  Background13()
  Background14()
  Background15()
  Background16()
  Background17()

  LinearGradient01()
  LinearGradient02()
  LinearGradient03()
  LinearGradient04()
  LinearGradient05()

  RadialGradient01()
  RadialGradient03()

  SweepGradient01()
  SweepGradient02()
  SweepGradient03()
  SweepGradient04()
  SweepGradient05()
  SweepGradient06()

  ClipTest02()
  ClipTest03()
  ClipTest04()
  ClipTest05()
  ClipTest06()
  ClipTest07()
  ClipTest08()
  ClipTest09()
  ClipTest10()
  MaskTest01()
  MaskTest02()
  MaskTest03()
  MaskTest04()
  MaskTest05()

  RotateTest01()
  RotateTest02()
  ScaleTest02()
  TranslateTest02()

  ClipTest01()
  ClipTest02()
  MaskTest01()

  OverlayTest01()
  OverlayTest02()
  OverlayTest03()
  OverlayTest04()
  OverlayTest05()

  VisibilityTest01()

  MatrixTest01()
  MatrixTest02()
  MatrixTest03()
  MatrixTest04()
  MatrixTest05()
  MatrixTest06()
  MatrixTest07()
  MatrixTest08()
  MatrixTest09()
  MatrixTest10()
  MatrixTest11()
  MatrixTest12()
  MatrixTest13()
  MatrixTest14()
  MatrixTest15()

  FocusTest01()
  FocusTest02()
  FocusTest03()

  ZIndexFont01()
  ZIndexFont06()
  ZIndexFont07()
  ZIndexFont08()
  ZIndexFont09()
  ZIndexFont10()
  ZIndexFont11()
  ZIndexFont12()
  ZIndexFont13()
  ZIndexFont14()
  ZIndexFont15()
  ZIndexFont16()
  ZIndexFont17()
  ZIndexFont18()

  TouchEvent01()
  TouchEvent02()
  TouchEvent03()
  TouchEvent04()
  TouchEvent05()

  BorderTest001()
  BorderTest002()
  BorderTest003()

  BackdropBlurTest001()
  BlurTest001()
  BrightnessTest001()
  BrightnessTest002()
  ColorBlendTest001()
  ContrastTest001()
  ContrastTest002()
  GrayscaleTest001()
  GrayscaleTest002()
  HueRotateTest001()
  HueRotateTest002()
  HueRotateTest003()
  InvertTest001()
  InvertTest002()
  SaturateTest001()
  SaturateTest002()
  SepiaTest001()
  SepiaTest002()
  ShadowTest001()

  LayTest001()
  LayTest002()
  LayTest003()
  LayTest004()
  LayTest005()
  LayTest006()
  LayTest007()

  AlignTest()
  AlignTest002()
  AlignRulesTest()
  MarkAnchorTest001()
  MarkAnchorTest002()
  OffsetTest()
  PositionTest()

  ResponseRegionTest001()
  ResponseRegionTest002()
  ResponseRegionTest003()
  ResponseRegionTest004()

  WidthTest001()
  WidthTest002()
  HeightTest001()
  HeightTest002()
  ConstraintSizeTest001()
  MarginTest001()
  MarginTest002()
  MarginTest003()
  MarginTest004()
  MarginTest005()
  MarginTest006()
  MarginTest007()
  MarginTest008()
  PaddingTest001()
  PaddingTest002()
  PaddingTest003()
  PaddingTest004()
  PaddingTest005()
  PaddingTest006()
  SizeTest001()
  SizeTest002()

  ClickTest001()
  ClickTest002()
  ClickTest003()

  FlexAlignContentTest001()
  FlexAlignItemsTest001()
  FlexAlignSelfTest001()
  FlexBasisTest001()
  FlexBasisTest002()
  FlexBasisTest003()
  FlexBasisTest004()
  FlexDirectionTest001()
  FlexDisplayPriority001()
  FlexGrowTest001()
  FlexGrowTest002()
  FlexGrowTest003()
  FlexJustifyContentTest001()
  FlexLayoutWeight001()
  FlexLayoutWeight002()
}